package okielegym.okielegymbackend.spring.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class DatabaseConfiguration {

    private final static String DATA_BASE_USER = "postgres";
    private final static String DATA_BASE_PASS = "kaczorek5";
    private final static String URL = "jdbc:postgresql://localhost:5432/okielegym";

    @Bean
    public static HikariDataSource getDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(URL);
        config.setUsername(DATA_BASE_USER);
        config.setPassword(DATA_BASE_PASS);
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        return new HikariDataSource(config);
    }

    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

}
