package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GYM")
public class Gyms {
    @Column(name = "id",unique = true)
    private int id;
    @Column(name = "address",length = 50,nullable = false)
    private String address;
    @Column(name = "sauna")
    private boolean sauna;
    @Column(name = "parking")
    private boolean parking;
}
