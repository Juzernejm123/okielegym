package okielegym.okielegymbackend.spring.configuration;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DatabaseConfiguration.class)
public class DatabaseConfigurationTest {

    @Test
    public void getDataSource() throws SQLException {
        System.out.println(DatabaseConfiguration.getConnection().getClientInfo());
        Assert.assertNotNull(DatabaseConfiguration.getConnection());
    }

    @Test
    public void getConnection() throws SQLException {
        Assert.assertNotNull(DatabaseConfiguration.getConnection());
    }
}
