package entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Trainer {
    @Column(name = "id", unique = true)
    private String id;
    @Column(name = "personal_trainer")
    private boolean personalTrainer;
}
