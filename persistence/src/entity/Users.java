package entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Users {
    @Column(name = "id", unique = true)
    private String id;
    @Column(name = "name", length = 30)
    private String name;
    @Column(name = "last_name", length = 30)
    private String lastName;
    @Column(name = "email", length = 40)
    private String email;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
